#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;

}

Point::Point(const Point &point) //costruttore del punto
{
    _x = point._x;
    _y = point._y;

}

double Point::getX() const
{
    return this->_x;
}

double Point::getY() const
{
    return this->_y;
}
Ellipse::Ellipse(const Point &center, const int &a, const int &b):
    _center(center)
{
    _a = a;
    _b = b;
}

double Ellipse::Area() const
{
    return _a*_b*M_PI;
}

Circle::Circle(const Point &center, const int &radius):

    Ellipse(center, radius, radius) {}


double Circle::Area() const
{
    return Ellipse::Area();
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3):
    //costruttore del triangolo, che inizializza gli attributi p1, p2, p3
    _p1(p1), _p2(p2), _p3(p3){}


double Triangle::Area() const
{
    double len12 = sqrt(pow(_p1.getX() - _p2.getX(), 2) + pow(_p1.getY() - _p2.getY(), 2));
    double len13 = sqrt(pow(_p1.getX() - _p3.getX(), 2) + pow(_p1.getY() - _p3.getY(), 2));
    double len32 = sqrt(pow(_p3.getX() - _p2.getX(), 2) + pow(_p3.getY() - _p2.getY(), 2));
    double semiPer = (len12 + len13 + len32)/2;
    //calcolo dell'area applicando la formula di Erone (avremmo anche potuto applicare la formula di Gauss)
    double res = sqrt(semiPer * (semiPer - len12) * (semiPer - len13) * (semiPer - len32));
    return res;
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):
    Triangle(p1, Point(0,0), Point(0,0))
{
    _edge = edge;
}

double TriangleEquilateral::Area() const
{
    return _edge * _edge * sqrt(3) / 4;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4):
    _p1(p1), _p2(p2), _p3(p3), _p4(p4) {}

double Quadrilateral::Area() const
{
    //suddivido il quadrilatero di partenza in 2 sotto-triangoli
    Triangle t = Triangle(_p1, _p2, _p3);
    Triangle s = Triangle(_p3, _p4, _p1);
    //calcoliamo ora l'area del quadrilatero come somma dei 2 sotto-triangoli t e s
    return t.Area() + s.Area();
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):
    Quadrilateral(p1, p2, p4, p2) {}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height):
    Parallelogram(p1, Point(0,0), Point(0,0))
{
    _base = base;
    _height = height;
}

double Rectangle::Area() const
{
    return _base * _height;
}

Square::Square(const Point &p1, const int &edge):
    Rectangle(p1, edge, edge) {}

}





















