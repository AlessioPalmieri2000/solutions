//si tratta di svolgere un programma di criptaggio.
//Forniti una parola ed un numero, si "shifta" ogni lettera del numero stesso.
//La parola viene passata come parametro da linea di comando.

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl; //viene lanciato un messaggio
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    //leggo da file mediante la funzione ifstream
    ifstream file;
    //la funzione open richiede in input il path del file da aprire
    file.open(inputFilePath);
    //leggo la prima riga del file mediante la funzione getline
    getline(file, text);

    file.close(); //una volta terminate le operazioni, il file deve essere ri-chiuso

  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    encryptedText = text;
    unsigned int temp; //definisco una nuova variabile temporanea
    unsigned int passLength = password.length();

    for(unsigned int i = 0; i < (unsigned int)text.length(); i++)
    {
        temp = text[i] + password[i % passLength]; //

        if(temp > 126) //se sommando, viene superato il limite massimo, torno all'inizio e riprendo a contare
            temp-=95; //dato da 127-32

        encryptedText[i] = temp;

       }

  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    decryptedText = text;
    int temp;
    unsigned int passLength = password.length();

    for(unsigned int i = 0; i < (unsigned int)text.length(); i++)
    {
        temp = text[i] - password[i % passLength]; //seguo il processo esattamente inverso del precedente

        if(temp < 32)

            temp += 95;

        decryptedText[i] = temp;

    }

  return true;
}
