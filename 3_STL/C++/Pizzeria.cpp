#include "Pizzeria.h"

namespace PizzeriaLibrary {

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    list<Ingredient>::iterator it;
    for (it = ingredients.begin(); it != ingredients.end(); it++) {
        if(name == (*it).Name)
            throw runtime_error("Ingredient already inserted");
        if(name < (*it).Name)
            break;
    }
    ingredients.insert(it, Ingredient(name, price, description));
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    for(list<Ingredient>::const_iterator it = ingredients.cbegin(); it != ingredients.cend(); it++) {
        if ((*it).Name == name) {
            return *it;
        }
    }
    throw runtime_error("Ingredient not found");

}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    Pizza new_pizza(name);

    for (list<Pizza>::iterator it = menu.begin(); it != menu.end(); it++) {
        if(name == (*it).Name)
            throw runtime_error("Pizza already inserted");
    }
    unsigned int numIngredients = ingredients.size();
    for(unsigned int i = 0; i < numIngredients; i++) {
        new_pizza.AddIngredient(FindIngredient(ingredients[i]));
    }
    menu.push_front(new_pizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    for (list<Pizza>::const_iterator it = menu.cbegin(); it != menu.cend(); it++) {
        if((*it).Name == name) {
            return (*it);
        }
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    int new_number = orders.size() + 1000;
    Order new_order;
    int num_pizzas = pizzas.size();
    new_order.InitializeOrder(num_pizzas);
    for (int i = 0; i < num_pizzas; i++) {
        new_order.AddPizza(FindPizza(pizzas[i]));
    }
    orders.push_back(new_order);
    return new_number;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    if (1000 + (int)orders.size() < numOrder + 1)
        throw runtime_error("Order not found");
    return orders[numOrder-1000];
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    Order selected_order = FindOrder(numOrder);
    string output = "";
    for (int i = 0; i < selected_order.NumPizzas(); i++) {
        output += "- " + selected_order.Pizzas[i].Name + ", " + to_string(selected_order.Pizzas[i].ComputePrice()) + " euro\n";
    }
    output += "  TOTAL: " + to_string(selected_order.ComputeTotal()) + " euro\n";
    return output;
}

string Pizzeria::ListIngredients() const
{
    string text_ingredients = "";
    for (list<Ingredient>::const_iterator it = ingredients.cbegin(); it != ingredients.cend(); it++) {
        text_ingredients += (*it).Name + " - '" + (*it).Description + "': " + to_string((*it).Price) + " euro" + "\n";
    }
    return text_ingredients;
}

string Pizzeria::Menu() const
{
    string text_menu = "";
    for (list<Pizza>::const_iterator it = menu.cbegin(); it != menu.cend(); it++) {
        text_menu += (*it).Name + " (" + to_string((*it).NumIngredients()) + " ingredients): " + to_string((*it).ComputePrice()) + " euro" + "\n";
    }
    return text_menu;
}

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    Ingredients.push_back(ingredient);
}

int Pizza::NumIngredients() const
{
    return Ingredients.size();
}

int Pizza::ComputePrice() const
{
    int price = 0;
    list<Ingredient>::const_iterator it;
    for (it = Ingredients.cbegin(); it != Ingredients.cend(); it++) {
        price += (*it).Price;
    }
    return price;
}

void Order::InitializeOrder(int numPizzas)
{
    if (numPizzas == 0)
        throw runtime_error("Empty order");
    Pizzas.reserve(numPizzas);
}

void Order::AddPizza(const Pizza &pizza)
{
    Pizzas.push_back(pizza);
}

const Pizza &Order::GetPizza(const int &position) const
{
    if (position>(int)Pizzas.size())
        throw runtime_error("Position passed is wrong");
    return Pizzas[position-1];
}

int Order::NumPizzas() const
{
    return Pizzas.size();
}

int Order::ComputeTotal() const
{
    int total = 0;
    for (int i = 0; i < NumPizzas(); i++) {
        total += Pizzas[i].ComputePrice();
    }
    return total;
}

}






































