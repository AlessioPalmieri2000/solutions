#include "Intersector2D2D.h"
// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    //definiamo le due rispettive tolleranze
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-5;

    //definiamo la matrice avente come prime 2 righe i vettori normali ai piani N1 e N2
    //la terza riga ha invece N1xN2, che è il vettore tangente alla linea di intersezione
    matrixNormalVector.setZero(); //settiamo inizialmente a zero tutte le sue entrate

    //definiamo ora il vettore contenente i parametri di traslazione dei piani (d1, d2, d3)
    rightHandSide.setZero(); //settiamo inizialmente a zero anche questa matrice
}
// ***************************************************************************

//Consideriamo il primo piano dell'intersezione
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}
// ***************************************************************************

//Consideriamo il secondo piano dell'intersezione
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}
// ***************************************************************************

//Calcoliamo direttamente l'intersezione tra i 2 piani
bool Intersector2D2D::ComputeIntersection()
{
    //definiamo il vettore tangente alla linea di intersezione mediante N1xN2
    tangentLine = (matrixNormalVector.row(0)).cross(matrixNormalVector.row(1));

    // verifichiamo se N_1 è parallelo a N_2; questo implica che i 2 piani sono paralleli
    //applicando la condizione di parallelismo
    if (tangentLine.squaredNorm() <= toleranceParallelism * toleranceParallelism)
    { // piani paralleli

        // controlliamo ora se i 2 piani sono coincidenti, quindi se il vettore di traslazione ha ratio of normal vectors
        // questo perchè piani paralleli possono essere multipli

        double ratio = 0;
        if (matrixNormalVector(0, 0) >= 1.0E-14)        //non è 0
            ratio = matrixNormalVector(0, 1) / matrixNormalVector(0, 0);
        else if (matrixNormalVector(0, 1) >= 1.0E-14)   // non è 0
            ratio = matrixNormalVector(1, 1) / matrixNormalVector(0, 1);
        else
            ratio = matrixNormalVector(1, 2) / matrixNormalVector(0, 2);
        //eccezion fatta per gli stessi multipli dei vettori normali, i parametri di traslazione sono uguali
        if (abs(rightHandSide(1) - rightHandSide(0) * ratio) <= 1.0E-16)
        {
            intersectionType = Coplanar; //allora i 2 piani sono coincidenti
            return false;
        }
        else
        {
            intersectionType = NoInteresection; //allora i 2 piani non si intersecano
            return false;
        }
    }

    // risolviamo il sistema sfruttando la fattorizzazione QR, partendo da un punto iniziale
    //colPivHouseHolderQr() calcola, data una matrice, la fattorizzazione QR
    pointLine = matrixNormalVector.colPivHouseholderQr().solve(rightHandSide);
    intersectionType = LineIntersection;
    return true;
}
