import math

class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y
#Point rappresenta una classe a sè

class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b

    def area(self) -> float:
        return self._a * self._b * math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super().__init__(center, radius, radius)
        self._radius = radius
        #super è una funzione per dare alle classi figlie i metodi del padre


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3

    def area(self) -> float:
        # Area = 0.5 * ||(p2 - p1) x (p3 - p1)||
        return 0.5 * abs(self._p1.X*self._p2.Y - self._p2.X*self._p1.Y + self._p2.X*self._p3.Y - self._p3.X*self._p2.Y + self._p3.X*self._p1.Y - self._p1.X*self._p3.Y)


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, Point(p1.X+edge, p1.Y), Point(p1.X+(0.5*edge), p1.Y+(edge*math.sqrt(3)/2)))
        self._edge = edge
        #edge è una variabile che identifica il lato



class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self) -> float:
        #Assuming ordered points, Area = 0.5 * ||(p2 - p1) x (p3 - p1)|| + 0.5 * ||(p3 - p1) x (p4 - p1)||
        return 0.5 * abs(self._p1.X*self._p2.Y + self._p2.X*self._p3.Y + self._p3.X*self._p4.Y + self._p4.X*self._p1.Y - self._p2.X*self._p1.Y - self._p3.X*self._p2.Y - self._p4.X*self._p3.Y - self._p1.X*self._p4.Y)


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super().__init__(p1, p2, Point(p2.X+p4.X-p1.X, p2.Y+p4.Y-p1.Y), p4)


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super().__init__(p1, Point(p1.X+base, p1.Y), Point(p1.X, p1.Y+height))
        #p1.X+ base perchè così trovo l'altro punto del parallelogramma
        self._base = base
        self._height = height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)
        self._edge = edge
