#include "Intersector2D1D.h"
// ***************************************************************************

//iniziamo l'esercizio definendo le opportune tolleranze, rispettivamente per l'intersezione e per il parallelismo
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism  = 1.0E-7;
}
// ***************************************************************************
Vector3d Intersector2D1D::IntersectionPoint()
{
    // se non vi è intersezione oppure non è ancora stata calcolata, lanciamo un messaggio di errore
    if (IntersectionType() != PointIntersection)
        throw runtime_error("There is no intersection");
    return *lineOriginPointer + intersectionParametricCoordinate*(*lineTangentPointer);
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal; //indica il vettore normale al piano
    planeTranslationPointer = &planeTranslation; //indica il parametro di traslazione (d) del piano
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin; //origine della retta
    lineTangentPointer = &lineTangent; //vettore tangente alla retta
}
// ***************************************************************************
//procediamo ora con il calcolo diretto delle intersezioni
bool Intersector2D1D::ComputeIntersection()
{
    //calcoliamo innanzitutto il prodotto scalare tra la normale ed il vettore tangente alla retta
    double normalDotTangent = (*planeNormalPointer).dot(*lineTangentPointer);
    //calcoliamo
    double normalDotDifferencePlaneLine = *planeTranslationPointer - (*planeNormalPointer).dot(*lineOriginPointer);
    //qui controlliamo se il piano normale n è perpendicolare al vettore tangente, che implica che la retta ed il piano sono fra loro paralleli
    if (-toleranceParallelism <= normalDotTangent && normalDotTangent <= toleranceParallelism)
    {
        // controlliano se n.dot(punto_del_piano - origine_della_retta) = n.dot(punto_del_piano) - n.dot(origine_della_retta) = traslazione_piano - n.dot(origine_della_retta) = 0
        // questo implica che la retta è contenuta nel piano
        if (-toleranceParallelism <= normalDotDifferencePlaneLine && normalDotDifferencePlaneLine <= toleranceParallelism)
        {
            intersectionType = Coplanar; //in questo caso, la retta è contenuta nel piano, dunque ritorniamo "false"
            return false;
        }
        else
        {
            intersectionType = NoInteresection; //in questo secondo caso, non vi sono intersezioni tra la retta ed il piano, dunque ritorniamo ancora "false"
            return false;
        }
    }
    intersectionType = PointIntersection; //vi sono intersezioni tra la retta ed il piano

    //calcoliamo così il valore dell'ascissa curvilinea mediante il rapporto tra il prodotto scalare tra la normale al piano e la retta che congiunge 2 punti del piano
    //ed il prodotto scalare tra la normale al piano ed il vettore tangente
    double s = normalDotDifferencePlaneLine / normalDotTangent;
    intersectionParametricCoordinate = s; //poniamo infine uguale a s il valore della coordinata parametrica di intersezione
    return true;
}






