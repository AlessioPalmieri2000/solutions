# include "viaMichelin.h"
#include <iostream>
#include <fstream>
#include <sstream>
namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;
void BusStation::ResetBusStation()
{
    _numberBuses = 0;
    _buses.clear();
}
void BusStation::Load()
{
    // loads BusStation attributes reading a file
    ResetBusStation();
    ifstream file;
    file.open(_busFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");
    try {
        string line;
        // number buses
        getline(file, line);    // skip comment
        getline(file, line);
        istringstream numberConverter;
        numberConverter.str(line);
        numberConverter >> _numberBuses;
        // buses
        getline(file, line);    // skip comment
        getline(file, line);    // skip comment
        _buses.reserve(_numberBuses);
        for (int b = 0; b < _numberBuses; b++)
        {
            int id, fuelCost;
            getline(file, line);
            istringstream fuelConverter;
            fuelConverter.str(line);
            fuelConverter >> id >> fuelCost;
            _buses.push_back(Bus{ id, fuelCost });
        }
    }  catch (exception) {
        ResetBusStation();
        throw runtime_error("Something goes wrong");
    }
}
const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus <= 0 || idBus > _numberBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");
    return _buses[idBus - 1];
}
int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    int totalTravelTime = 0;
    const Route& route = _mapData.GetRoute(idRoute);
    for (int s = 0; s < route.NumberStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        totalTravelTime += street.TravelTime;
    }
    return totalTravelTime;
}
int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    double totalCost = 0.0;
    const Route& route = _mapData.GetRoute(idRoute);
    const Bus& bus = _busStation.GetBus(idBus);
    for (int s = 0; s < route.NumberStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        totalCost += (double)street.TravelTime * (double)bus.FuelCost * ((double)BusAverageSpeed / 3600.0);
    }
    return (int)totalCost;
}
string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    ostringstream routeView;
    routeView << idRoute << ": ";
    for (int s = 0; s < route.NumberStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        const BusStop& from = _mapData.GetStreetFrom(street.Id);
        routeView<< from.Name<< " -> ";
        // because at the last call we have to insert from and to the create the route
        if (s == route.NumberStreets - 1)
        {
            const BusStop& to = _mapData.GetStreetTo(street.Id);
            routeView<< to.Name;
        }
    }
    return routeView.str();
}
string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);
    return to_string(idStreet) +
            ": " +
            from.Name +
            " -> " +
            to.Name;
}
string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);
    return busStop.Name +
            " (" +
            to_string((double)busStop.Latitude / 10000.0) +
            ", " +
            to_string((double)busStop.Longitude / 10000.0) +
            ")";
}
void MapData::Reset()
{
    _numberRoutes = 0;
    _numberStreets = 0;
    _numberBusStops = 0;
    _routes.clear();
    _streets.clear();
    _busStops.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();
}
void MapData::Load()
{
    // loads MapData attribute reading a file
    Reset();
    ifstream file;
    file.open(_mapFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");
    try {
        string line;
        // numberBusStops
        getline(file, line);    // skip comment
        getline(file, line);
        istringstream numberBusStopConverter;
        numberBusStopConverter.str(line);
        numberBusStopConverter >> _numberBusStops;
        // BusStops
        getline(file, line);    // skip comment
        _busStops.reserve(_numberBusStops);
        for (int b = 0; b < _numberBusStops; b++)
        {
            int id, latitude, longitude;
            string name;
            getline(file, line);
            istringstream busStopConverter;
            busStopConverter.str(line);
            busStopConverter >> id >> name >> latitude >> longitude;
            _busStops.push_back(BusStop{id, latitude, longitude, name});
        }
        // numberStreets
        getline(file, line);    // skip comment
        getline(file, line);
        istringstream numberStreetsConverter;
        numberStreetsConverter.str(line);
        numberStreetsConverter >> _numberStreets;
        // Streets
        getline(file, line);    // skip comment
        _streets.reserve(_numberStreets);
        _streetsFrom.reserve(_numberStreets);
        _streetsTo.reserve(_numberStreets);
        for (int s = 0; s < _numberStreets; s++)
        {
            int id, from, to, travelTime;
            getline(file, line);
            istringstream streetConverter;
            streetConverter.str(line);
            streetConverter >> id >> from >> to >> travelTime;
            _streets.push_back(Street{id, travelTime});
            _streetsFrom.push_back(from);
            _streetsTo.push_back(to);
        }
        // numberRoutes
        getline(file, line);    // skip comment
        getline(file, line);
        istringstream numberRoutesConverter;
        numberRoutesConverter.str(line);
        numberRoutesConverter >> _numberRoutes;
        // Routes
        getline(file, line);    // skip comment
        _routes.reserve(_numberRoutes);
        _routeStreets.reserve(_numberRoutes);
        for (int r = 0; r < _numberRoutes; r++)
        {
            int id, streetNumber;
            getline(file, line);
            istringstream RoutesConverter;
            RoutesConverter.str(line);
            RoutesConverter >> id >> streetNumber;
            _routes.push_back(Route{id, streetNumber});
            // routeStreets is vector<vector<int>> so we add another vector and fill it
            // each vector<int> shows the route with all the streets
            _routeStreets.push_back(vector<int>());
            _routeStreets[r].reserve(streetNumber);
            for (int n = 0; n < streetNumber; n++)
            {
                int idStreet;
                RoutesConverter >> idStreet;
                _routeStreets[r].push_back(idStreet);
            }
        }
    }  catch (exception) {
        Reset();
        throw runtime_error("Something goes wrong");
    }
}
const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    const Route& route = GetRoute(idRoute);
    if (streetPosition < 0 || streetPosition > route.NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");
    // get idStreet
    int idStreet = _routeStreets[idRoute - 1][streetPosition];
    // use it to get the street
    return _streets[idStreet - 1];
}
const Route &MapData::GetRoute(const int &idRoute) const
{
    if(idRoute <= 0 || idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    return _routes[idRoute - 1];
}
const Street &MapData::GetStreet(const int &idStreet) const
{
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    return _streets[idStreet - 1];
}
const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    int idBus = _streetsFrom[idStreet - 1];
    return _busStops[idBus - 1];
}
const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if(idStreet <= 0 || idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    int idBus = _streetsTo[idStreet - 1];
    return _busStops[idBus - 1];
}
const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if(idBusStop <= 0 || idBusStop > _numberBusStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
    return _busStops[idBusStop - 1];
}
}
