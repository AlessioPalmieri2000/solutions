#iniziamo con la descrizione della classe degli ingredienti necessari per la successiva preparazione della pizza
class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        #il costruttore della classe degli ingredienti, con nome, prezzo e descrizione
        self.Name = name
        self.Price = price
        self.Description = description

#si procede con la classe della pizza
class Pizza:
    def __init__(self, name: str):
        #il costruttore della classe pizza, con nome ed ingredienti
        self.Name = name
        #tutti gli ingredienti vengono raccolti in una lista
        self.Ingredients = []

    #per poi passare alla funzione che aggiunge gli ingredienti
    def addIngredient(self, ingredient: Ingredient):
        #la funzione append permette di inserire gli elementi in coda ad una lista
        self.Ingredients.append(ingredient)

    #questo metodo restituisce il numero degli elementi
    def numIngredients(self) -> int:
        #la funzione len restituisce il numero di ingredienti nella lista
        return len(self.Ingredients)

    #questo metodo calcola il prezzo totale degli ingredienti nella lista
    def computePrice(self) -> int:
        price = 0
        for ing in self.Ingredients:
            price += ing.Price
        return price

#si passa alla classe degli ordini
class Order:
    def __init__(self):
        #le pzze vengono inserite in una lista
        self.Pizzas = []

    def getPizza(self, position: int) -> Pizza:
        if(position > len(self.Pizzas)):
            #lanciamo un messaggio di errore
            raise ValueError("Position passed is wrong")
        return self.Pizzas[position-1]

    def initializeOrder(self, numPizzas: int):
        if numPizzas == 0:
            raise ValueError("Empty order")

    def addPizza(self, pizza: Pizza):
        size: int = len(self.Pizzas)
        #questa funzione ci permette di aggiungere una pizza in coda alla lista
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        #restituiamo il numero di pizza della lista
        return len(self.Pizzas)

    #calcoliamo il prezzo totale dell'ordine
    def computeTotal(self) -> int:
        total: int = 0
        #mediante un ciclo for si calcola il prezzo totale di tutte le pizze
        for pizza in self.Pizzas:
            total += pizza.computePrice()
        return total

#concludiamo con la classe "cardine" dell'esercizio
class Pizzeria:
    def __init__(self):
        self.__ingredients = []
        self.__menu = []
        self.__orders = []

    def addIngredient(self, name: str, description: str, price: int):
        index: int = 0
        for element in self.__ingredients:
            if element.Name == name:
                #lanciamo un messaggio di errore
                raise ValueError("Ingredient already inserted")
            if (element.Name > name):
                index = self.__ingredients.index(element)
        self.__ingredients.insert(index, Ingredient(name, price, description))


    def findIngredient(self, name: str) -> Ingredient:
        for element in self.__ingredients:
            if element.Name == name:
                return element
        raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        new_pizza = Pizza(name)
        for pizza in self.__menu:
            if pizza.Name == name:
                raise ValueError("Pizza already inserted")
        for current_ing in ingredients:
            new_pizza.addIngredient(self.findIngredient(current_ing))
        self.__menu.append(new_pizza)

    #bisogna trovare la pizza nell'elenco
    def findPizza(self, name: str) -> Pizza:
        for pizza in self.__menu:
            if pizza.Name == name:
                return pizza
        raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        new_number: int = len(self.__orders) + 1000
        new_order = Order()
        new_order.initializeOrder(len(pizzas))
        for pizza in pizzas:
            new_order.addPizza(self.findPizza(pizza))
        self.__orders.append(new_order)
        return new_number

    def findOrder(self, numOrder: int) -> Order:
        if (1000 + len(self.__orders) < numOrder + 1):
            raise ValueError("Order not found")
        return self.__orders[numOrder - 1000]

    def getReceipt(self, numOrder: int) -> str:
        selected_order: Order = self.findOrder(numOrder)
        output: str = ""
        #riportiamo il formato necessario per l'output
        for current_pizza in selected_order.Pizzas:
            output += "- " + current_pizza.Name + ", " + str(current_pizza.computePrice()) + " euro\n"
        output += " TOTAL: " + str(selected_order.computeTotal()) + " euro\n"
        return output

    def listIngredients(self) -> str:
        text: str = ""
        for current_ingredient in self.__ingredients:
            #anche in questo caso si riporta il formato del testo
            text += current_ingredient.Name + " - '" + current_ingredient.Description + "': " + str(current_ingredient.Price) + " euro" + "\n"
        return text

    def menu(self) -> str:
        text_menu: str = ""
        for pizza in self.__menu:
            #si riporta il formato del menu
            text_menu += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) + " euro" + "\n"
        return text_menu
